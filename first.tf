provider "aws" {
  access_key = "<your_access_key>"
  secret_key = "<your_secret_key>"
  region = "eu-central-1"
}

resource "aws_instance" "true-server-01" {
  ami = "ami-065deacbcaac64cf2"
  instance_type = "t2.micro"
  user_data = "${file("userdata.sh")}"
}

data "aws_subnets" "true-subnets" { 
}

data "aws_vpc" "true-vpc" {
}

data "aws_instances" "true-servers" {
}

resource "aws_lb" "true-lb" {
  name               = "true-lb"
  internal           = false
  load_balancer_type = "network"
  subnets            = data.aws_subnets.true-subnets.ids

}

resource "aws_lb_listener" "trueserver-lb-listener-http" {
  load_balancer_arn = aws_lb.true-lb.arn
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg-http.arn
  }
}

resource "aws_lb_listener" "trueserver-lb-listener-ssh" {
  load_balancer_arn = aws_lb.true-lb.arn
  port              = "22"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg-ssh.arn
  }
}


resource "aws_lb_listener" "trueserver-lb-listener-https" {
  load_balancer_arn = aws_lb.true-lb.arn
  port              = "443"
  protocol          = "TLS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:eu-central-1:719914593525:certificate/9ee4f9d3-1b15-40c6-b440-0e144ed17d6b"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg-http.arn
  }
}

resource "aws_lb_target_group" "tg-ssh" {
  name     = "tg-ssh-true-server"
  port     = 22
  protocol = "TCP"
  vpc_id   = data.aws_vpc.true-vpc.id
}

resource "aws_lb_target_group" "tg-http" {
  name     = "tg-http-true-server"
  port     = 80
  protocol = "TCP"
  vpc_id   = data.aws_vpc.true-vpc.id
}


# resource "aws_lb_target_group" "tg-https" {
#   name     = "tg-https-true-server"
#   port     = 443
#   protocol = "TLS"
#   vpc_id   = data.aws_vpc.true-vpc.id
# }

resource "aws_lb_target_group_attachment" "ssh" {
  target_group_arn = aws_lb_target_group.tg-ssh.arn
  port             = 22

  count = length(data.aws_instances.true-servers.ids)
  target_id = data.aws_instances.true-servers.ids[count.index]
}


resource "aws_lb_target_group_attachment" "http" {
  target_group_arn = aws_lb_target_group.tg-http.arn
  port             = 80

  count = length(data.aws_instances.true-servers.ids)
  target_id = data.aws_instances.true-servers.ids[count.index]
}

# resource "aws_lb_target_group_attachment" "https" {
#   target_group_arn = aws_lb_target_group.tg-https.arn
#   port             = 443

#   count = length(data.aws_instances.true-servers.ids)
#   target_id = data.aws_instances.true-servers.ids[count.index]
# }

data "aws_route53_zone" "true_alb_zone" {
  name   = "welbex.smeshelper.ru"
}

resource "aws_route53_record" "alb_alias" {
  zone_id = data.aws_route53_zone.true_alb_zone.zone_id
  name    = ""
  type    = "A"

  alias {
    name                   = aws_lb.true-lb.dns_name
    zone_id                = aws_lb.true-lb.zone_id
    evaluate_target_health = true
  }
}



